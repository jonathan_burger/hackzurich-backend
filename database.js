var mongojs = require('mongojs');

var db = mongojs('mongodb://hackzurich:hackzurich@ds059029.mongolab.com:59029/IbmCloud_9a3gj5tf_hb4j05ch', ['projects'], {authMechanism: 'ScramSHA1'})

exports.projects = db.collection('projects');
exports.users = db.collection('users');
exports.donations = db.collection('donations');