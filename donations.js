var express = require('express');
var Promise = require('bluebird');
var _ = require('underscore');
var validator = require('validator');
var donationDb = Promise.promisifyAll(require('./database').donations);
var projectDb = Promise.promisifyAll(require('./database').projects);
var userDb = Promise.promisifyAll(require('./database').users);
var auth = require('./auth').auth;
var mongojs = require('mongojs');

exports.router = express.Router();

exports.router.post('/', auth);
exports.router.post('/', function (request, response) {
	var data = {};
	var _project = null;
	Promise.try(function () {
		if (!_.isObject(request.body)) {
			throw new Error('Invalid request format');
		}
		if (!_.isString(request.body.project_id)) {
			throw new Error('Invalid Project ID');
		}
		if (!_.isFinite(parseFloat(request.body.amount))) {
			throw new Error('Need to specify amount.');
		}
		if (parseFloat(request.body.amount) > request.user.credit) {
			throw new Error('Not enough credit.');
		}
		if (parseFloat(request.body.amount) == 0) {
			throw new Error('Must donate at least 0.01');
		}
		data.amount = parseFloat(request.body.amount);
		return new Promise(function (resolve, reject) {
			projectDb.findOne({_id: mongojs.ObjectId(request.body.project_id)}, function (err, response) {
				if (err) { reject(err); }
				else { resolve(response); }
			});
		});
	})
	.then(function (project) {
		if (!project) {
			throw new Error('Project not found.');
		}
		_project = project;
		data.project = mongojs.ObjectId(request.body.project_id);
		data.donator = request.user._id;
		return new Promise(function (resolve, reject) {
			donationDb.save(data, function (err, result) {
				if (err) { reject(err); }
				else { resolve(result); }
			})
		});
	})
	.then(function (result) {
		request.user.credit -= parseFloat(request.body.amount);
		return new Promise(function (resolve, reject) {
			userDb.save(request.user, function (err, result) {
				if (err) { reject (err); }
				else { resolve(result); }
			})
		})
	})
	.then(function (user) {
		_project.amount += parseFloat(request.body.amount);
		return new Promise(function (resolve, reject) {
			projectDb.save(_project, function (err, result) {
				if (err) { reject(err); }
				else { resolve(result); }
			})
		})
	})
	.then(function () {
		response.json({
			success: true
		})
	})
	.catch(function (err) {
		response.json({
			success: false,
			error: err.message
		})
	});
});