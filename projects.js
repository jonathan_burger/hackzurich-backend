var express = require('express');
var db = require('./database');
var _ = require('underscore');
var Promise = require('bluebird');
var auth = require('./auth').auth;
var optionalauth = require('./auth').optionalauth;
var ta = require('time-ago')();


exports.router = express.Router();

exports.router.get('/', optionalauth);
exports.router.get('/', function (request, response) {
    db.projects.find().sort({_id: -1}, function (err, results) {
        creators = _.uniq(_.map(results, function (result) {
            return result.creator;
        }));
        db.users.find({_id: {$in: creators}}, function (err, users) {
            results = _.map(results, function (result) {
                result.creator = _.filter(users, function (user) {
                    return user._id.toString() === result.creator.toString();
                })[0];
                result.creator = {
                    id: result.creator._id,
                    username: result.creator.username
                }
                result.date = ta.ago(new Date(parseInt(result._id.toString().substring(0, 8), 16) * 1000));
                result.can_donate = request.user ? (result.creator._id.toString() != request.user._id.toString()) : false;
                result.is_creator = request.user ? (result.creator._id.toString() == request.user._id.toString()) : false;
                return result;
            });
            response.json(results);
        })
    });
});

exports.router.post('/', auth);
exports.router.post('/', function (request, response) {
    var project = request.body;
    var projectObj = {};
    Promise.try(function() {
        if (!_.isObject(request.body)) {
            throw new Error('Invalid request format');
        }
        if (!_.isString(request.body.name)) {
            throw new Error('Need to specify project name');
        }
        projectObj.name = request.body.name;
        if (!_.isString(request.body.description)) {
            throw new Error('Need to specify project description');

        }
        projectObj.description = request.body.description;
        if (!_.isString(request.body.image)) {
            throw new Error('Need to specify image URL');
        }
        projectObj.image = request.body.image;
        projectObj.creator = request.user._id;
        projectObj.amount = 0;
        return new Promise(function (resolve, reject) {
            db.projects.save(projectObj, function (err, result) {
                if (err) {  reject (err); }
                else { resolve(result); }
            });
        })
    })
    .then(function (project) {
        response.json({
              id: projectObj._id,
              name: projectObj.name,
              description: projectObj.description,
              image: projectObj.image,
              date: ta.ago(new Date(parseInt(result._id.toString().substring(0, 8), 16) * 1000)),
              creator: {
                  id: request.user._id,
                  username: request.user.username
              }
        });
    })
    .catch(function (err) {
      response.status(400);
      response.json({
        message: err.message
      })
    })
   });
