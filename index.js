var express = require('express');
var mongojs = require('mongojs');
var db = require('./database');
var bodyParser = require('body-parser');

var userRouter = require('./user');
var projectRouter = require('./projects');
var donationsRouter = require('./donations');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.raw());

app.get('/', function  (request, response) {
	response.end('Hackathon 2015');
});

app.use('/api/projects', projectRouter.router);
app.use('/api/user', userRouter.router);
app.use('/api/donations', donationsRouter.router);

app.listen(process.env.PORT || 9000);