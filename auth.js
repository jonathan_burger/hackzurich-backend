var Promise = require('bluebird');
var _ = require('underscore');
var userDb = Promise.promisifyAll(require('./database').users);

exports.auth = function (request, response, next) {
	Promise.try(function () {
		if (!_.isObject(request.query)) {
			throw new Error('Authentication required.');
		}
		if (!_.isString(request.query.token)) {
			throw new Error('No token specified.');
		}
		return new Promise(function (resolve, reject) {
			userDb.findOne({token: request.query.token}, function (err, res) {
				if (err) { reject(err); }
				else { resolve(res); }
			})
		})
	})
	.then(function (user) {
		request.user = user;
		next()
	})
	.catch(function (err) {
		response.json({
			success: false,
			error: err.message
		})
	});
}

exports.optionalauth = function (request, response, next) {
	if (!request.query.token) {
		next();
		return;
	}
	Promise.try(function () {
		if (!_.isString(request.query.token)) {
			throw new Error('No token specified.');
		}
		return new Promise(function (resolve, reject) {
			userDb.findOne({token: request.query.token}, function (err, res) {
				if (err) { reject(err); }
				else { resolve(res); }
			})
		})
	})
	.then(function (user) {
		request.user = user;
		next()
	})
	.catch(function (err) {
		response.json({
			success: false,
			error: err.message
		})
	});
}