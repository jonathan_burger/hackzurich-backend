var express = require('express');
var Promise = require('bluebird');
var _ = require('underscore');
var validator = require('validator');
var userDb = Promise.promisifyAll(require('./database').users);
var passwordHash = require('password-hash');
var randomstring = require('randomstring');
var auth = require('./auth').auth;
exports.router = express.Router();

exports.router.get('/', function(request, response) {
	response.end('ok')
})
exports.router.post('/register', function (request, response) {
	var userObj = {}
	Promise.try(function() {
		if (!_.isObject(request.body)) {
			throw new Error('Invalid request format');
		}
		if (!_.isString(request.body.username)) {
			throw new Error('No username specified')
		}
		if (request.body.username.length > 32) {
			throw new Error('Username cannot be longer than 32 characters');
		}
		if (request.body.username.length < 3) {
			throw new Error('Username must be at least 3 characters');
		}
		userObj.username = request.body.username;
		if (!_.isString(request.body.password)) {
			throw new Error('Need to fill in password');
		}
		if (request.body.password.length < 8) {
			throw new Error('Password needs to be at least 8 characters long.');
		}
		userObj.password = passwordHash.generate(request.body.password);
		if (!validator.isEmail(request.body.email)) {
			throw new Error('Invalid email address');
		}
		userObj.email = request.body.email;
		userObj.token = randomstring.generate(64);
		userObj.credit = 500;
		return new Promise(function (resolve, reject) {
			userDb.findOne({username: request.body.username}, function (err, res) {
				if (err) { reject(err); }
				else { resolve(res); }
			})
		})
	})
	.then(function (user) {
		if (user) {
			throw new Error('Username is already taken.');
		}
		return new Promise(function (resolve, reject) {
			userDb.save(userObj, function (err, res) {
				if (err) { reject(err); }
				else { resolve(res); }
			});
		});
	})
	.then(function (saved) {
		response.json({
			success: true,
			token: userObj.token
		})
	})
	.catch(function (err) {
		response.json({
			success: false,
			error: err.message
		})
	});
});


exports.router.post('/login', function (request, response) {
	Promise.try(function () {
		if (!_.isObject(request.body)) {
			throw new Error('Invalid request');
		}
		if (!_.isString(request.body.username)) {
			throw new Error('Need to provide username')
		}
		if (!_.isString(request.body.password)) {
			throw new Error('Need to provide password')
		}
		return new Promise(function (resolve, reject) {
			userDb.findOne({username: request.body.username}, function (err, res) {
				if (err) { reject(err); }
				else { resolve(res); }
			})
		})
	})
	.then(function (user) {
		if (!user) {
			throw new Error('Username/Password combination wrong.');
		}
		if (passwordHash.verify(request.body.password, user.password)) {
			response.json({
				success: true,
				token: user.token
			});
		}
		else {
			throw new Error('Username/Password combination wrong.');
		}
	})
	.catch(function (err) {
		response.json({
			success: false,
			error: err.message
		})
	});
});

exports.router.get('/me', auth);
exports.router.get('/me', function (request, response) {
	response.json({
		success: true,
		user: {
			username: request.user.username,
			credit: request.user.credit,

		}
	})
});